package config

import (
	"fmt"
	"net/url"
	"strconv"
	"strings"
)

type Protocol string

const (
	ProtocolTCP Protocol = "tcp"
	ProtocolUDP Protocol = "udp"
)

type Network struct {
	Original string
	Protocol Protocol
	Port     int
	Address  string
}

func (n Network) URL() string {
	return fmt.Sprintf("%s://%s", n.Protocol, n.Addr())
}

func (n Network) Addr() string {
	return fmt.Sprintf("%s:%d", n.Address, n.Port)
}

func (n Network) StringPort() string {
	return fmt.Sprintf("%d", n.Port)
}

func NetworkFromString(strcfg string) (Network, error) {
	u, err := url.Parse(strcfg)
	if err != nil {
		return Network{}, err
	}

	port, _ := strconv.ParseInt(u.Port(), 10, 32)

	return Network{
		Original: strcfg,
		Protocol: Protocol(u.Scheme),
		Port:     int(port),
		Address:  u.Hostname(),
	}, nil
}

type Config struct {
	Local  Network
	Remote Network
}

func FromString(strcfg string) (Config, error) {
	urls := strings.Split(strcfg, " ")
	if len(urls) != 2 {
		return Config{}, fmt.Errorf("requires two urls")
	}
	localNetwork, err := NetworkFromString(urls[0])
	if err != nil {
		return Config{}, fmt.Errorf("wrong local url: %w", err)
	}
	remoteNetwork, err := NetworkFromString(urls[1])
	if err != nil {
		return Config{}, fmt.Errorf("wrong remote url: %w", err)
	}

	return Config{
		Local:  localNetwork,
		Remote: remoteNetwork,
	}, nil
}
