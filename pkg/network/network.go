package network

import (
	"fmt"
	"goportforward/pkg/config"
	"io"
	"log"
	"net"
)

func Forward(conn1, conn2 net.Conn) {
	stop := make(chan bool, 2)

	// read from client then write to remote
	go func() {
		defer func() { stop <- true }()
		_, err := io.Copy(conn2, conn1)
		log.Printf("client to remote close: %v", err)
	}()
	// read from remote then write to client
	go func() {
		defer func() { stop <- true }()
		_, err := io.Copy(conn1, conn2)
		log.Printf("remote to client close: %v", err)
	}()
	<-stop
}

func Listen(local config.Network) (net.Listener, error) {
	localproto := string(local.Protocol)
	localaddr := fmt.Sprintf("%s:%d", local.Address, local.Port)

	listener, err := net.Listen(localproto, localaddr)
	return listener, err
}

func Dial(remote config.Network) (net.Conn, error) {
	remoteconn, err := net.Dial(string(remote.Protocol), remote.Addr())
	if err != nil {
		return nil, fmt.Errorf("cannot connect to remote: %w", err)
	}
	return remoteconn, nil
}

type Forwarder struct {
	Config config.Config
}

func NewForwarder(network config.Config) *Forwarder {
	l := Forwarder{
		Config: network,
	}
	return &l
}

func (l *Forwarder) Start() error {
	listener, err := Listen(l.Config.Local)
	if err != nil {
		return fmt.Errorf("cannot listen: %w", err)
	}

	log.Printf("started forwarder: %v -> %s", l.Config.Local.URL(), l.Config.Remote.URL())

	for {
		if clientconn, err := listener.Accept(); err != nil {
			log.Printf("cannot accept connection: %v", err)
			continue
		} else {
			go func() {
				defer clientconn.Close()
				log.Printf("accepted connection: %v <- %v", clientconn.LocalAddr(), clientconn.RemoteAddr())
				remoteconn, err := Dial(l.Config.Remote)
				if err != nil {
					log.Printf("cannot connect to remote: %v", err)
				} else {
					Forward(clientconn, remoteconn)
				}
			}()
		}
	}
}
