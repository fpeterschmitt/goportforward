package client

import (
	"goportforward/pkg/config"
	"goportforward/pkg/network"
	server2 "goportforward/pkg/server"
	"log"
	"net"
)

func Client(forwardConn net.Conn, server config.Network, cfg config.Config) {
	serverConn, err := net.Dial(string(server.Protocol), server.Addr())
	if err != nil {
		log.Fatalf("cannot connect to server: %v", err)
	}
	defer serverConn.Close()

	log.Printf("connected: %v -> %v -> %v -> %v",
		forwardConn.RemoteAddr(), forwardConn.LocalAddr(),
		serverConn.LocalAddr(), serverConn.RemoteAddr())

	if _, err := serverConn.Write([]byte(server2.ServerIntro + cfg.Remote.Original)); err != nil {
		log.Fatalf("cannot write to server: %v", err)
	}

	buf := make([]byte, 8192)
	if n, err := serverConn.Read(buf); err != nil {
		log.Fatalf("cannot read server: %v", err)
	} else if n > 0 {
		str := string(buf[:n])
		if str == "%GOPORTFORWARD%ok" {
			network.Forward(forwardConn, serverConn)
		} else {
			log.Println(str)
		}
	} else {
		log.Println(string(buf))
	}
}
