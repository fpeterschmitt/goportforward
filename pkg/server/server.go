package server

import (
	"goportforward/pkg/config"
	"goportforward/pkg/network"
	"log"
	"net"
)

const ServerIntro = "%GOPORTFORWARD%1%"

func Serve(clientServerConn net.Conn) {
	buf := make([]byte, 8192)
	n, err := clientServerConn.Read(buf)
	if err == nil && n > len(ServerIntro) {
		cfgstr := string(buf[len(ServerIntro):n])
		if cfg, err := config.NetworkFromString(cfgstr); err != nil {
			clientServerConn.Write([]byte("%GOPORTFORWARD%error:" + err.Error()))
		} else {
			clientServerConn.Write([]byte("%GOPORTFORWARD%ok"))
			remoteConn, err := network.Dial(cfg)
			if err != nil {
				clientServerConn.Write([]byte("%GOPORTFORWARD%error:" + err.Error()))
				return
			}
			log.Printf("proxy forward %v -> %v", clientServerConn.RemoteAddr(), cfg.Original)
			network.Forward(clientServerConn, remoteConn)
		}
	} else if err != nil {
		log.Printf("cannot read: %v", err)
		clientServerConn.Write([]byte("%GOPORTFORWARD%error:" + err.Error()))
	} else {
		clientServerConn.Write([]byte("%GOPORTFORWARD%error:botched\n"))
	}
}
