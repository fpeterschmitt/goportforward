# goportforward

 * Go
 * Port
 * Forwarding

What else?

## Usage example

```
❭ go build ./cmd/goportforward/ && ./goportforward -cfg "tcp://localhost:8000 tcp://localhost:5432 | tcp://localhost:8001 tcp://localhost:5432"
2022/08/24 21:39:25 started forwarder: tcp://localhost:8001 -> tcp://localhost:5432
2022/08/24 21:39:25 started forwarder: tcp://localhost:8000 -> tcp://localhost:5432
2022/08/24 21:39:39 accepted connection: 127.0.0.1:8001 <- 127.0.0.1:53747
```

```
psql -h localhost -p 8001 -U user -W database
```


boom.
