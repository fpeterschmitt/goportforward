package main

import (
	"flag"
	"goportforward/pkg/client"
	"goportforward/pkg/config"
	"goportforward/pkg/network"
	server2 "goportforward/pkg/server"
	"log"
	"strings"
)

func clientMode(server config.Network, cfg config.Config) {
	localConn, err := network.Listen(cfg.Local)
	if err != nil {
		log.Fatalf("client cannot listen: %v", err)
	}
	defer localConn.Close()

	log.Printf("client listens: %v", localConn.Addr())

	for {
		if forwardConn, err := localConn.Accept(); err != nil {
			log.Printf("cannot accept: %v", err)
		} else {
			go func() {
				defer forwardConn.Close()
				client.Client(forwardConn, server, cfg)
			}()
		}
	}
}

func serverMode(server config.Network) {
	serverConn, err := network.Listen(server)
	if err != nil {
		log.Fatalf("server cannot listen: %v", err)
	}
	defer serverConn.Close()

	log.Println("server listens:", serverConn.Addr())

	for {
		if clientServerConn, err := serverConn.Accept(); err != nil {
			log.Printf("cannot accept: %v", err)
		} else {
			go func() {
				defer clientServerConn.Close()
				server2.Serve(clientServerConn)
			}()
		}
	}
}

func standaloneMode(cfgs []config.Config) {
	stop := make(chan bool, len(cfgs))
	for _, cfg := range cfgs {
		go func(cfg config.Config) {
			defer func() {
				log.Println("one forwarder down")
				stop <- true
			}()
			if err := network.NewForwarder(cfg).Start(); err != nil {
				log.Printf("cannot start forwarder: %v", err)
			} else {
				log.Println("forwarder terminated")
			}
		}(cfg)
	}
	<-stop
}

func main() {
	clicfg := flag.String("cfg", "empty", "strictly respect spaces: 'protocol://local:port protocol://remote:port | ...'")
	client := flag.String("client", "", "works like ssh -L. needs one element in -cfg")
	server := flag.String("server", "", "provide address to listen to: tcp://0.0.0.0:8000")
	flag.Parse()

	strcfgs := strings.Split(*clicfg, " | ")
	if strcfgs[0] == "empty" {
		strcfgs = []string{}
	}

	cfgs := make([]config.Config, 0, len(strcfgs))

	for i, strcfg := range strcfgs {
		cfg, err := config.FromString(strcfg)
		if err != nil {
			log.Fatalf("wrong config %d: %v", i, err)
		}
		cfgs = append(cfgs, cfg)
	}

	if len(*client) > 0 && len(cfgs) != 1 {
		log.Fatalf("when using -client, use exactly one element in -cfg")
	}

	if len(*client) == 0 && len(*server) == 0 {
		standaloneMode(cfgs)
	} else if len(*client) > 1 && len(*server) == 0 {
		netclient, err := config.NetworkFromString(*client)
		if err != nil {
			log.Fatalf("wrong server url: %v", err)
		}
		clientMode(netclient, cfgs[0])
	} else if len(*server) > 1 && len(*client) == 0 {
		netserver, err := config.NetworkFromString(*server)
		if err != nil {
			log.Fatalf("wrong server url: %v", err)
		}
		serverMode(netserver)
	} else {
		log.Fatalf("invalid mode")
	}

}
